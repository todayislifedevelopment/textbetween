<!-- @format -->

# textbetween

Extracts the text between two tags in a string.

-   @param string $starttag The start tag of the text to extract.
-   @param string $endtag The end tag of the text to extract.
-   @param string $string The string to be searched.
-   @param int $occurrence The number of the tag to extract from (default: 1).
-   @param bool $force Specifies whether the function should return an empty result if the end tag is not found (default: true).
-
-   @return string The extracted text between the tags or an empty string if $force is set to false and the end tag is not found.

## Features

-   Respects nested tags

## Installation

composer require todayislifedevelopment/textbetween

## Usage/Examples

```php
require_once('textbetween');

$string = "<p>First</p>Nothing<p>Second<p>Next Level</p></p>Rest";

echo textbetween('<p>', '</p>', $string) . "\n";           // "First"
echo textbetween('<p>', '</p>', $string, 1) . "\n";        // "First"
echo textbetween('<p>', '</p>', $string, 1, false) . "\n"; // "First"
echo textbetween('<p>', '</p>', $string, 2) . "\n";        // "Second<p>Next Level</p>"
echo textbetween('<p>', '</p>', $string, 3) . "\n";        // "Next Level"
echo textbetween('<p>', '</p>', $string, 4) . "\n";        // ""
echo textbetween('<test>', '</test>', $string) . "\n";     // ""
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## License

The MIT License (MIT). Please see [License File](LICENSE.txt) for more information.

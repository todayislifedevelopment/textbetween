<?php

declare(strict_types=1);

namespace todayislifedevelopment;

use \InvalidArgumentException;

/**
 * Sucht den Text zwischen Start- und End-Tag in einem String.
 *
 * @param string $starttag Das Start-Tag
 * @param string $endtag Das End-Tag
 * @param string $string Der String, in dem der Text gesucht wird
 * @param int $occurrence Die Nummer des Vorkommens des Start-Tags (Standard: 1)
 * @param bool $keepEmpty Gibt an, ob ein leerer String zurückgegeben werden soll, wenn das End-Tag nicht gefunden wurde (Standard: true)
 *
 * @return string|null Der Text zwischen Start- und End-Tag oder null, wenn das End-Tag nicht gefunden wurde
 *
 * @throws InvalidArgumentException Wenn $starttag, $endtag oder $string leer sind oder $occurrence kleiner als 1 ist
 */

final class TextBetween
{
    static string $starttag = '';
    static string $endtag = '';
    static string $string = '';
    static int $occurrence = 1;
    static bool $keepEmpty = true;

    public function __construct(string $starttag, string $endtag, string $string, int $occurrence = 1, bool $keepEmpty = true)
    {
        if (empty($starttag) || empty($endtag) || empty($string) || $occurrence < 1) {
            throw new InvalidArgumentException('Ungültige Parameter');
        }
        self::$starttag = $starttag;
        self::$endtag = $endtag;
        self::$string = $string;
        self::$occurrence = $occurrence;
        self::$keepEmpty = $keepEmpty;
    }

    public static function withString(string $string): string
    {
        self::$string = $string;
        return self::parse(self::$occurrence);
    }

    public static function withOccurence(int $occ): string
    {
        self::$occurrence = $occ;
        return self::parse(self::$occurrence);
    }

    public static function withDelimiters(string $start, string $end): string
    {
        self::$starttag = $start;
        self::$endtag = $end;
        return self::parse(self::$occurrence);
    }

    public static function replaceWith(string $string): string
    {
        $pos = self::parseString(false);
        self::$string =
            substr(self::$string, 0, $pos['start'] - 1) .
            $string .
            substr(self::$string, $pos['end']);
        return self::$string;
    }

    public static function parse($occurrence): string
    {
        if (isset($occurrence)) self::$occurrence = $occurrence;
        return self::parseString();
    }

    private static function parseString($asString = true): string|array
    {
        $delimiters = [self::$starttag, self::$endtag];
        $splittedString = self::splitStringByDelimiters(self::$string, $delimiters);

        $nStart = -1;
        $nEnd = -1;
        $nOpen = 0;
        $result = ''; // Ergebnis
        $resultPos = ['start' => 0, 'end' => 0];
        $counter = 0;
        $nItem = 0;
        foreach ($splittedString as $item) {

            if ($nStart == -1) {
                if ($item['text'] == self::$starttag) {
                    $counter++;
                    if ($counter == self::$occurrence) {
                        $nStart = $nItem;
                        $resultPos['start'] = $item['start'];
                    }
                }
            } else {        // Nun wird $endtag gesucht
                if ($item['text'] == self::$starttag) {
                    $result .= $item['text'];
                    $nOpen++;
                } elseif ($item['text'] == self::$endtag) {
                    if ($nOpen == 0) {
                        $nEnd = $nItem;
                        $resultPos['end'] = $item['end'] + strlen(self::$endtag);
                        break;
                    } else {
                        $result .= $item['text'];
                        $nOpen--;
                    }
                } else {
                    $result .= $item['text'];
                }
            }
            $nItem++;
        }
        if ($nEnd == -1 and !self::$keepEmpty) {
            $result = '';
        }
        if (!$asString) {
            return $resultPos;
        }

        return $result;
    }



    /**
     * Teilt einen String anhand eines Arrays von Trennzeichen auf und gibt ein Array mit den Substrings und den Positionen zurück.
     * 
     * @param string $string     Der zu trennende String.
     * @param array  $delimiters Ein Array von Trennzeichen.
     * 
     * @return array Ein Array von Substrings und Positionen.
     */
    private static function splitStringByDelimiters(string $string, array $delimiters = []): array
    {
        // Initialisiere das Ergebnis-Array
        $result = array();

        // Füge den Ausgangsstring am Anfang des Arrays hinzu, zusammen mit der Startposition
        $result[] = array(
            "text" => $string,
            "start" => 0,
        );

        // Schleife über alle Trenn-Strings
        foreach ($delimiters as $delimiter) {
            // Initialisiere das neue Ergebnis-Array
            $newResult = array();

            // Schleife über alle Teile des alten Ergebnis-Arrays
            foreach ($result as $part) {
                // Teile den Teilstring anhand des Trenn-Strings auf
                $split = explode($delimiter, $part["text"]);

                // Initialisiere die Startposition
                $start = $part["start"];

                // Schleife über alle Teile des aufgeteilten Strings
                foreach ($split as $subpart) {
                    // Bestimme die Länge des Subparts
                    $length = strlen($subpart);

                    // Füge das Subpart zum neuen Ergebnis-Array hinzu, zusammen mit der Startposition
                    $newResult[] = array(
                        "text" => $subpart,
                        "start" => $start,
                    );

                    // Falls dies kein letztes Subpart ist, füge auch den Trenn-String hinzu, zusammen mit der Startposition
                    if ($subpart !== end($split)) {
                        $newResult[] = array(
                            "text" => $delimiter,
                            "start" => $start + $length,
                        );
                        // Erhöhe die Startposition um die Länge des Subparts und des Trenn-Strings
                        $start += $length + strlen($delimiter);
                    } else {
                        // Erhöhe die Startposition nur um die Länge des Subparts
                        $start += $length;
                    }
                }
            }

            // Überschreibe das alte Ergebnis-Array mit dem neuen
            $result = $newResult;
        }

        // Entferne eventuelle leere Elemente im Array
        $result = array_filter($result, function ($value) {
            return $value["text"] !== '';
        });

        // Gebe das Ergebnis-Array zurück
        return $result;
    }
}
